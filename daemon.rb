require 'rubygems'
require 'daemons'

app_dir = Dir.pwd
Daemons.run_proc('server.rb', {:dir_mode => :normal, :dir => app_dir}) do
  Dir.chdir(app_dir)
  exec "ruby server.rb"
end
