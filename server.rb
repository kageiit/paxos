require 'sinatra'
require 'sinatra/config_file'
require 'dalli'
require 'open-uri'
require 'fileutils'
require 'cgi'

set :cache, Dalli::Client.new
set :wait_time, 0.005
config_file 'config.yml'

#when server receives READ request
get '/READ' do
  content_type 'text'
  File.open(settings.file, "rb") do |f|
    f.flock(File::LOCK_EX)
    @content = f.read
    f.flock(File::LOCK_UN)
  end
  return @content
end

#when server receives APPEND request
get '/APPEND' do
  data = params[:data]
  tid = params[:tid]
    
  #check if paxos leader
  if settings.self == settings.paxos_leader
    #record transaction and start tpc
    set(tid, data) #transaction is only set in the paxos leaders which take part in tpc
    
    #check if tpc coordinator
    if settings.self == settings.coordinator
      
      #ad-hoc queueing for write operations on coordinator
      jid = job_id() #get a new job_id
      cid = curr_job() #current job id
      while jid != cid #wait for turn in queue
        sleep(settings.wait_time)
        cid = curr_job() #current job id
      end
      
      #tpc phase 1
      response = ''
      settings.tpc_nodes.split(',').each do |node| #send commit_prepare to all nodes in tpc
        req = node+'/commit_prepare?tid='+tid
        response = response + open(req).read
      end
      
      #tpc phase 2 - commit or abort
      if response == 'AA' #all nodes in tpc acknowledge this transaction , therefore commit it
        response = ''
        settings.tpc_nodes.split(',').each do |node| #send commit message to all nodes in tpc for this transaction
          response = response + open(node+'/commit?tid='+tid).read
        end
      else #in case of failure
        response = ''
        settings.tpc_nodes.split(',').each do |node| #send abort message to all nodes in tpc for this transaction
          response = response + open(node+'/abort?tid='+tid).read
        end
      end
      
      #process next job in queue
      next_job()
      
      #remove transaction 
      delete(tid)
      
      if response == 'AA'
        return tid+': committed' #transaction committed successfully
      else #aborted
        return tid+': aborted'
      end
    else #cohort, return nothing
      return tid+': waiting for coordinator'
    end
    
  else #if not leader, forward request to leader and act as proxy
    return open(settings.paxos_leader+'/APPEND?tid='+tid+'&data='+CGI::escape(data)).read
  end
  
end

#2 Phase Commit part

#when a node receives a commit request
get '/commit_prepare' do
  tid = params[:tid]
  data = get(tid)
  
  while data == nil #wait till data for transaction is received at cohort
    sleep(settings.wait_time)
    data = get(tid)
  end
  
  if data != nil #if transaction is valid
    #trigger accept phase of paxos, as only leader receives commit_prepare
    response = ''
    n = next_n()
    settings.paxos_nodes.split(',').each do |node| #send accept message to all nodes in paxos cluster
      response = response + open(node+'/accept?n='+n+"&val="+CGI::escape(data)).read
    end
    
    if response.count('A') >= 2 #if majority of nodes replicated successfully
      return 'A'
    else #could not get majority of nodes to agree
      return 'N'
    end
  else #no transaction data before timeout
    return 'N'
  end
end

get '/commit' do
  tid = params[:tid]
  data = get(tid)
  response = ''
  settings.paxos_nodes.split(',').each do |node| #send decide to all nodes in paxos cluster
    response = response + open(node+'/decide').read
  end
  return 'A'
end

get '/abort' do
  #roll back to old version
  FileUtils.cp(settings.file, settings.file+'.BAK')
  return 'B'
end

#Paxos part

#when node receives an accept message (multi-paxos)
get '/accept' do
  req_n = params[:n].to_i
  req_val = params[:val]
  prev_n = get('acc_n')
  
  if(req_n >= prev_n)
    set('acc_n', req_n)
    set('acc_val', req_val)

    #replicate the request value
    open(settings.file+'.BAK', 'a') { |f|
      f.puts(req_val)
    }
    
    return 'A'
  else
    return 'N'
  end
end

get '/decide' do
  #finalize commit
  FileUtils.move(settings.file+'.BAK', settings.file)
  FileUtils.cp(settings.file, settings.file+'.BAK')
  return 'A'
end

get '/reset' do
  #init server state
  flush()
  set('acc_n', 0, ttl=settings.long_ttl+rand(100))
  set('n', 0, ttl=settings.long_ttl+rand(100), options={:raw => true})
  set('job_id', 0, ttl=settings.long_ttl+rand(100), options={:raw => true})
  set('curr_job', 1, ttl=settings.long_ttl+rand(100), options={:raw => true})
end

def next_n()
  settings.cache.incr('n').to_s
end

def set(key, val, time_to_live=settings.long_ttl, options=nil)
  settings.cache.set(key, val, ttl=time_to_live+rand(100), options)
end

def get(key)
  settings.cache.get(key)
end

def flush()
  settings.cache.flush()
end

def job_id()
  settings.cache.incr('job_id').to_i
end

def curr_job()
  settings.cache.get('curr_job').to_i
end

def next_job()
  settings.cache.incr('curr_job').to_i
end

def delete(key)
  settings.cache.delete(key)
end