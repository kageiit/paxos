#!/usr/bin/python

from urllib import urlencode
from urllib2 import urlopen
import time, uuid
from threading import Thread, Lock
from optparse import OptionParser
import os, re

read_re = re.compile("\"?(read east-grades|read east-stats|read eu-grades|read eu-stats|read west-grades|read west-stats)\"?")
append_re = re.compile("\"?append .+?\:.+\"?")

class MyParser(OptionParser):
  def format_epilog(self, formatter):
    return self.epilog

parser = MyParser(epilog=
"""Examples:
read east-grades
read eu-stats
read west-grades
append 01 02 03 04 05 06 07 08 09 10:01 10 05
""")
parser.add_option('-t', '--threads', action="store_true", dest="enable_threads", default=False, help="Enable parallel append queries")
parser.add_option('-f', '--file', dest="file", default=None,help="input file with append data")
parser.add_option('-c', '--command', dest="command", default=None,help="Perform a single operation")
options, args = parser.parse_args()

servers = {'east-grades' : 'http://ec2-54-242-125-15.compute-1.amazonaws.com:4567',
           'east-stats' : 'http://ec2-50-16-162-135.compute-1.amazonaws.com:4567',
           'eu-grades' : 'http://ec2-46-51-165-43.eu-west-1.compute.amazonaws.com:4567',
           'eu-stats' : 'http://ec2-54-246-55-89.eu-west-1.compute.amazonaws.com:4567',
           'west-grades' : 'http://ec2-50-112-70-243.us-west-2.compute.amazonaws.com:4567',
           'west-stats' : 'http://ec2-50-112-200-126.us-west-2.compute.amazonaws.com:4567',
           }

coordinator = 'east-grades'
cohort = 'eu-stats'
lock =Lock()

def append(server, tid, data):
  data = urlencode({'tid': tid, 'data': data})
  url = servers[server]+'/APPEND?'+data
  print 'Append Transaction: ',tid
  resp = urlopen(url).read()
  resp = 'SERVER: '+server+'\nRESPONSE:\n'+resp
  return resp

def read(server):
  url = servers[server]+'/READ'
  resp = urlopen(url).read()
  resp = 'SERVER: '+server+'\nRESPONSE:\n'+resp
  return resp

class Bot(Thread):
  def __init__(self, server, tid, data):
    Thread.__init__(self)
    self.server = server
    self.tid = tid
    self.data = data
    
  def run(self):
    resp = append(self.server, self.tid, self.data)
    
    #print responses
    lock.acquire()
    try:
      print resp
    finally:
      lock.release()
    
def process(test_data):
  bots = []
  for d in test_data:
    grades, stats = d.split(':')
    tid = str(uuid.uuid4())
    
    if options.enable_threads:
      bots.append(Bot(cohort, tid, stats))
      bots.append(Bot(coordinator, tid, grades))
    else:
      print append(cohort, tid, stats)
      print append(coordinator, tid, grades)

  for bot in bots:
    bot.start()
        
def main():
  if options.file:
    if not os.path.exists(options.file):
      print options.file,'does not exist'
      exit(1)
    else:
      test_data = open(options.file, 'r').read().split('\n')
      test_data.remove('')
      process(test_data)
  elif options.command:
    command = options.command.lower()
    is_read = re.findall(read_re, command)
    if is_read:
      print read(is_read[0][5:])
      return
    is_append = re.findall(append_re, command)
    if is_append:
      process([is_append[0][7:]])
      return
  else:
    while True:
      op = None
      while op not in ['READ', 'APPEND', 'EXIT']:
        print "Enter valid operation: (READ/APPEND/EXIT)"
        op = raw_input().upper()
      if op == 'READ':
        server = None
        s = servers.keys()
        while server not in s: 
          print "Enter valid server:",'/'.join(s)
          server = raw_input().lower()
        print read(server)
      elif op == 'APPEND':
        print "Enter Grades:",
        grades = raw_input()
        print "Enter Stats:",
        stats = raw_input()
        process([grades+':'+stats])
      elif op == 'EXIT':
        break
      
        
if __name__ == '__main__':
  main()
